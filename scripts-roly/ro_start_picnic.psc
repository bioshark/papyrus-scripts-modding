Scriptname ro_start_picnic extends ObjectReference  

ObjectReference Property foldedBlanket Auto
ObjectReference Property openedBlanket Auto
ObjectReference Property specialMarker01 Auto
ObjectReference Property specialMarker02 Auto


Event OnActivate(ObjectReference player)
	If (player == Game.GetPlayer())		
		if (foldedBlanket.IsEnabled())
			; start picnic
			openedBlanket.Enable();
			specialMarker01.Enable();
            specialMarker02.Enable();
			foldedBlanket.Disable();
		else
			; stop picnic
			openedBlanket.Disable();
			specialMarker01.Disable();
            specialMarker02.Disable();
			foldedBlanket.Enable();
		endif
	endif
endEvent