Scriptname disable_painting extends ObjectReference

ObjectReference property pPainting auto
Keyword Property Vampire  Auto

Event OnCellAttach()
	If Game.GetPlayer().HasKeyword(Vampire)
		pPainting.Disable()
	else
		pPainting.Enable()
	EndIf
Endevent