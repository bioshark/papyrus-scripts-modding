Scriptname enable_garden_lights extends ObjectReference

Float Property fHourToDisable Auto
Float Property fHourToEnable Auto
ObjectReference Property pLightSourceOn Auto
ObjectReference Property pLightSourceOff Auto
ObjectReference Property pGardenMarker Auto

Function runActions()
	If (fHourToDisable < GetCurrentHourOfDay() && GetCurrentHourOfDay() < fHourToEnable)
		GoToState("LightsOff")
	Else
		GoToState("LightsOn")
	EndIf
EndFunction

Event OnInit()
	runActions()
EndEvent

Function RegisterForSingleUpdateGameTimeAt(float GameTime)
	float CurrentTime = GetCurrentHourOfDay()
	If (GameTime < CurrentTime)
		GameTime += 24
	EndIf
	float updateInHours = GameTime - CurrentTime;
	If (updateInHours > 24)
		updateInHours -=24
	EndIf
	RegisterForSingleUpdateGameTime(updateInHours)
EndFunction

State LightsOff
	Event OnBeginState()
		If (pGardenMarker.IsDisabled())
			pLightSourceOn.Disable()
			pLightSourceOff.Disable()
		else
			pLightSourceOn.Disable()
			pLightSourceOff.Enable()
		EndIf
		RegisterForSingleUpdateGameTimeAt(fHourToEnable)
	EndEvent
	Event OnUpdateGameTime()
		GoToState("LightsOn")
	EndEvent
EndState
 
State LightsOn
	Event OnBeginState()
		If (pGardenMarker.IsDisabled())
			pLightSourceOn.Disable()
			pLightSourceOff.Disable()
		else
			pLightSourceOn.Enable()
			pLightSourceOff.Disable()
		EndIf
		RegisterForSingleUpdateGameTimeAt(fHourToDisable)
	EndEvent
	Event OnUpdateGameTime()
		GoToState("LightsOff")
	EndEvent
EndState

float Function GetCurrentHourOfDay() global
	float Time = Utility.GetCurrentGameTime()
	Time -= Math.Floor(Time)
	Time *= 24
	Return Time
EndFunction
