Scriptname ro_enable_garden_lights_v2 extends ObjectReference

Float Property fHourToDisable Auto
Float Property fHourToEnable Auto
ObjectReference Property pLightSourceOn Auto
ObjectReference Property pLightSourceOff Auto
ObjectReference Property pGardenMarker Auto

Event OnInit()
	GoToState("LightsOnOff")
EndEvent

Event onCellAttach()
	GoToState("LightsOnOff")
EndEvent

Function RegisterLightSwitch()
	float currentTime = GetCurrentHourOfDay()
	float updateInHours
	if ((fHourToDisable < currentTime) && (currentTime <= fHourToEnable))
		updateInHours = fHourToEnable - currentTime;
	elseif ((fHourToEnable < currentTime) && (currentTime <= 24))
		updateInHours = 24 - currentTime + fHourToDisable;
	elseif (currentTime < fHourToDisable)
		updateInHours = fHourToDisable - currentTime;
	endif
	RegisterForSingleUpdateGameTime(updateInHours)
EndFunction

float Function GetCurrentHourOfDay() global
	float Time = Utility.GetCurrentGameTime()
	Time -= Math.Floor(Time)
	Time *= 24
	Return Time
EndFunction

Function turnLightsOnOff()
	If (pGardenMarker.IsEnabled())
		If (fHourToDisable < GetCurrentHourOfDay() && GetCurrentHourOfDay() < fHourToEnable)
			pLightSourceOn.Disable()
			pLightSourceOff.Enable()
		Else
			pLightSourceOn.Enable()
			pLightSourceOff.Disable()
		EndIf
	else
		pLightSourceOn.Disable()
		pLightSourceOff.Disable()
	EndIf
EndFunction

State LightsOnOff
	Event OnBeginState()
		turnLightsOnOff()
		RegisterLightSwitch()
	EndEvent
	Event OnUpdateGameTime()
		GoToState("LightsOnOff")
	EndEvent
EndState