Scriptname Display_Weapon_Script extends ObjectReference  

Weapon Property ClawToPlace Auto
Message Property FailMessage auto

Event OnActivate(ObjectReference akActivator)
	if (self.getLinkedRef().isEnabled())
		self.getLinkedRef().disable();
		(akActivator as actor).addItem(ClawToPlace, 1);
	else
		if akActivator.getItemCount(ClawToPlace) >= 1
			self.getLinkedRef().enable();
			(akActivator as actor).removeItem(ClawToPlace, 1);
		else
			FailMessage.show();
		endif
	endif
endEvent