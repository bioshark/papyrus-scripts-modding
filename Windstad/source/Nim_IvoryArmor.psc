Scriptname Nim_IvoryArmor extends ObjectReference  

ObjectReference property IvoryCuirass auto
ObjectReference property IvoryBoots auto
ObjectReference property IvoryGauntlets auto
ObjectReference property IvoryCrown auto


ObjectReference property Ivory_chest auto


Form DLC1IvoryCuirass
Form DLC1IvoryBoots
Form DLC1IvoryGauntlets
Form DLC1IvoryCrown



Event OnInit()

       DLC1IvoryCuirass= Game.GetFormFromFile(0x00c816,"Dawnguard.esm")
       DLC1IvoryBoots= Game.GetFormFromFile(0x00c815,"Dawnguard.esm")
       DLC1IvoryGauntlets= Game.GetFormFromFile(0x00c817,"Dawnguard.esm")
       DLC1IvoryCrown= Game.GetFormFromFile(0x00c814,"Dawnguard.esm")


    
EndEvent       

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
    
    If akBaseItem==DLC1IvoryCuirass
        IvoryCuirass.enable()
    elseif akBaseItem==DLC1IvoryBoots
        IvoryBoots.enable()
    elseif akBaseItem==DLC1IvoryGauntlets
        IvoryGauntlets.enable()
    elseif akBaseItem==DLC1IvoryCrown
        IvoryCrown.enable()

    endif
   
   
Endevent

Event OnItemRemoved(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
   
    
    If akBaseItem==DLC1IvoryCuirass
        IvoryCuirass.disable()
    elseif akBaseItem==DLC1IvoryBoots
        IvoryBoots.disable()
    elseif akBaseItem==DLC1IvoryGauntlets
        IvoryGauntlets.disable()
    elseif akBaseItem==DLC1IvoryCrown
        IvoryCrown.disable()

    endif


Endevent
