Scriptname ro_UniquesB_script extends ObjectReference  

Book Property Unique Auto
Message Property FailMessage Auto

Event OnActivate(ObjectReference akActivator)
	if (self.getLinkedRef().isEnabled())
		self.getLinkedRef().disable();
		(akActivator as actor).addItem(Unique, 1);
	else
		if akActivator.getItemCount(Unique) >= 1
			self.getLinkedRef().enable();
			(akActivator as actor).removeItem(Unique, 1);
		else
			FailMessage.show();
		endif
	endif
endEvent