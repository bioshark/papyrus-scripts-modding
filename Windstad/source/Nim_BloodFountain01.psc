Scriptname Nim_BloodFountain01 extends ObjectReference  

Actor Property Player auto
Potion Property BloodPotion auto


Event OnActivate(ObjectReference akActivator)
    if akActivator == game.GetPlayer() as ObjectReference
        Player.EquipItem(BloodPotion, false, true)
	endif
EndEvent