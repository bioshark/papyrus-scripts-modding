Scriptname Ro_WInnMenu extends ObjectReference

Message Property menuMain Auto

Actor Property PlayerREF Auto
Actor Property innkeeper Auto

GlobalVariable Property glHireStuff Auto
GlobalVariable Property glOpenInn Auto
GlobalVariable Property glExtendGuests Auto

GlobalVariable[] Property glPersons Auto
ObjectReference[] Property refPersons Auto

ObjectReference Property treasureChest Auto

MiscObject property gold001 Auto

Faction Property JobInnkeeperFaction Auto

int income = 0 ;

Event OnActivate(ObjectReference akActionRef)
    If akActionRef == PlayerREF ; Only the player
        Menu()
    EndIf
EndEvent

Event OnCellAttach()
    HandleTavernProperties();
    HandlePersons();
EndEvent

Event OnInit()
    HandleTavernProperties();
    HandlePersons();
EndEvent

Function Menu(Bool abMenu = True, Int aiButton = 0)
    While abMenu
        If aiButton != -1
            aiButton = menuMain.Show() ; Main Menu
            If aiButton == 0 ; Hire staff
                If (PlayerREF.GetItemCount(gold001) >= 3500)
                    PlayerREF.RemoveItem(gold001, 3500)
                    setGlobalValue(glHireStuff, 1.0);
                    setGlobalValue(glPersons[0], 1.0);
                    setGlobalValue(glPersons[1], 1.0);
                    setGlobalValue(glPersons[2], 1.0);
                Else
                    debug.MessageBox("You can't afford to do that.");                
                EndIf    
            ElseIf aiButton == 1; Fire staff
                setGlobalValue(glHireStuff, 0.0);
                setGlobalValue(glPersons[0], 0.0);
                setGlobalValue(glPersons[1], 0.0);
                setGlobalValue(glPersons[2], 0.0);
            ElseIf aiButton == 2; Open inn
                RegisterForUpdateGameTime(24.0)
                setGlobalValue(glOpenInn, 1.0);
                setGlobalValue(glPersons[3], 1.0);
                setGlobalValue(glPersons[4], 1.0);
                setGlobalValue(glPersons[5], 1.0);
            ElseIf aiButton == 3; Extend guests
                setGlobalValue(glExtendGuests, 1.0);
                setGlobalValue(glPersons[6], 1.0);
                setGlobalValue(glPersons[7], 1.0);
                setGlobalValue(glPersons[8], 1.0);
            ElseIf aiButton == 4; Close inn
                setGlobalValue(glOpenInn, 0.0);
                setGlobalValue(glPersons[3], 0.0);
                setGlobalValue(glPersons[4], 0.0);
                setGlobalValue(glPersons[5], 0.0);
                income = 0 ;
            ElseIf aiButton == 5; Reduce guests
                setGlobalValue(glExtendGuests, 0.0);
                setGlobalValue(glPersons[6], 0.0);
                setGlobalValue(glPersons[7], 0.0);
                setGlobalValue(glPersons[8], 0.0);
            ElseIf aiButton == 6; Exit
                abMenu = False ; Exit
            EndIf
        EndIf
    EndWhile
EndFunction

Function setGlobalValue(GlobalVariable gv, float value)
    gv.SetValue(value)
EndFunction

Function HandlePersons()
    int index = glPersons.length;
    While index
        index -= 1;
        If glPersons[index].getValue() == 1.0
            refPersons[index].enable();
        Else
            refPersons[index].disable();        
        EndIf
    EndWhile
EndFunction

Event OnUpdateGameTime()
    int valueToAdd = 0 ;
    If glOpenInn.getValue() == 1.0
        valueToAdd += Utility.RandomInt(75, 90) + Utility.RandomInt(75, 90) + Utility.RandomInt(75, 90) ;
        If glExtendGuests.getValue() == 1.0
            valueToAdd += Utility.RandomInt(75, 90) + Utility.RandomInt(75, 90) + Utility.RandomInt(75, 90) ;
        EndIf
    EndIf
    
    income += valueToAdd;
    If income > 0
        treasureChest.AddItem(gold001, income);
        income = 0;
    EndIf
            
EndEvent

Function HandleTavernProperties()
    if (GetCurrentHourOfDay() >= 8.00)
        innkeeper.AddToFaction(JobInnkeeperFaction);
    elseif (GetCurrentHourOfDay() <= 1.00)
       innkeeper.AddToFaction(JobInnkeeperFaction);
    else
        innkeeper.RemoveFromFaction(JobInnkeeperFaction);
    endif
EndFunction

float Function GetCurrentHourOfDay() global
	float Time = Utility.GetCurrentGameTime()
	Time -= Math.Floor(Time)
	Time *= 24
	Return Time
EndFunction
