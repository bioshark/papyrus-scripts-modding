Scriptname Ro_BloodBath extends ObjectReference  

Actor Property PlayerREF Auto

GlobalVariable Property globalMaidenBought Auto
GlobalVariable Property globalLeverBloodPulled Auto

ObjectReference property refIronMaidenOne Auto
ObjectReference property refIronMaidenTwo Auto
ObjectReference property refBloodWater Auto
ObjectReference property refWaterLow Auto
ObjectReference Property refMaidenOne Auto
ObjectReference Property refMaidenTwo Auto
ObjectReference Property refDeadMaidenOne Auto
ObjectReference Property refDeadMaidenTwo Auto
ObjectReference Property refGoreMarker Auto
ObjectReference Property refWaterHigh Auto
ObjectReference Property refPositionMarkerLeft Auto
ObjectReference Property refPositionMarkerRight Auto

Sound Property soundTrapSpringIn Auto
Sound Property soundTrapSpringOut Auto
Sound Property soundDeathSound01 Auto
Sound Property soundDeathSound02 Auto

Spell Property spellFortify Auto

Event OnActivate(ObjectReference akActionRef)
    If akActionRef == PlayerREF
        if (globalMaidenBought.GetValue() == 1.0 && globalLeverBloodPulled.GetValue() == 0.0)
            if refWaterHigh.isEnabled()
                refWaterHigh.disable();
            endif
            refIronMaidenOne.SetOpen(false);
            refIronMaidenTwo.SetOpen(false);        
            Utility.wait(2.0);
            refMaidenOne.disable();
            refMaidenTwo.disable();
            refDeadMaidenOne.MoveTo(refPositionMarkerLeft);
            refDeadMaidenTwo.MoveTo(refPositionMarkerRight);
            refGoreMarker.enable();
            globalMaidenBought.SetValue(0.0);
            soundTrapSpringIn.play(self)
            Utility.wait(0.5);
            soundDeathSound01.play(self)
            Utility.wait(1.0);
            soundDeathSound02.play(self)
            Utility.wait(0.5);
            soundTrapSpringOut.play(self);        
            refBloodWater.enable();
            refWaterLow.disable();
            Utility.wait(6.0);
            spellFortify.Cast(PlayerREF);
            debug.notification("You feel rejuvenated");
            globalLeverBloodPulled.SetValue(1.0);
        elseif (globalLeverBloodPulled.GetValue() == 1.0) ;clean-up
            if refBloodWater.IsEnabled()
                refBloodWater.Disable()
            endif
            if refWaterLow.IsDisabled()
                refWaterLow.Enable()
            endif
            globalLeverBloodPulled.SetValue(0.0);
        else
            debug.MessageBox("Unavailable at this time!");
        EndIf
    EndIf
EndEvent