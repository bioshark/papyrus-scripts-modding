Scriptname Ro_TavernStop extends ObjectReference

Actor Property innkeeper Auto
Faction Property JobInnkeeperFaction Auto

Event OnInit()

EndEvent

Event OnCellAttach()
    HandleTavernProperties();
EndEvent

Function HandleTavernProperties()
    if (GetCurrentHourOfDay() >= 8.00)
        innkeeper.AddToFaction(JobInnkeeperFaction);
    elseif (GetCurrentHourOfDay() <= 1.00)
       innkeeper.AddToFaction(JobInnkeeperFaction);
    else
        innkeeper.RemoveFromFaction(JobInnkeeperFaction);
    endif
EndFunction

float Function GetCurrentHourOfDay() global
	float Time = Utility.GetCurrentGameTime()
	Time -= Math.Floor(Time)
	Time *= 24
	Return Time
EndFunction
