Scriptname Ro_DisplayLogsOnEnter extends ObjectReference

Actor Property PlayerREF Auto
ObjectReference Property theTrigger Auto
MiscObject Property BYOHMaterialLog Auto

Event OnTriggerLeave(ObjectReference akActionRef)
    theTrigger.GetBaseObject().SetName("(" + PlayerREF.GetItemCount(BYOHMaterialLog) + ")");
EndEvent
