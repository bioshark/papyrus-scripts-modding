Scriptname Nim_Player_uses_idle extends ObjectReference  


ObjectReference Property MarkerREF Auto 
Actor Property PlayerRef Auto 


Event OnActivate(ObjectReference akActionRef) 

	if akActionRef == PlayerREF
		MarkerREF.Activate(PlayerREF)
		
	endif
EndEvent
