Scriptname ro_BathNormal extends ObjectReference  

Actor Property PlayerREF Auto

GlobalVariable Property globalLeverWaterPulled Auto

ObjectReference property refWaterLow Auto
ObjectReference property refWaterHigh Auto
ObjectReference property refBloodWater Auto

Event OnActivate(ObjectReference akActionRef) 
    If akActionRef == PlayerREF
        if globalLeverWaterPulled.GetValue() == 0.0
            if refBloodWater.isEnabled()
                refBloodWater.disable();
            endif
            refWaterLow.disable();
            refWaterHigh.enable();
            globalLeverWaterPulled.SetValue(1.0);
        elseif globalLeverWaterPulled.GetValue() == 1.0
            if refWaterHigh.IsEnabled()
                refWaterHigh.disable()
            endif
            if refWaterLow.IsDisabled()
                refWaterLow.Enable()
            endif            
            globalLeverWaterPulled.SetValue(0.0);
        endif
    EndIf
EndEvent