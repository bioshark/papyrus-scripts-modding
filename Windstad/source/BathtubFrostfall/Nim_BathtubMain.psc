Scriptname Nim_BathtubMain extends ObjectReference  

Import FrostUtil

GlobalVariable Property globalLeverBloodPulled Auto
GlobalVariable Property globalLeverWaterPulled Auto

ObjectReference Property xMarkerWarm Auto
ObjectReference Property xMarkerWaterLow Auto
ObjectReference Property xMarkerWaterHigh Auto
ObjectReference Property xMarkerWaterBlood Auto

Actor Property PlayerREF Auto
Armor property UndressAmulet auto


Event OnTriggerEnter(ObjectReference akActionRef) 
    actor npc = akActionRef as actor
    npc.unequipall();
    xMarkerWarm.Enable()
    ModPlayerWetness(580.0)
EndEvent


Event OnTriggerLeave(ObjectReference akActionRef) 
    if xMarkerWaterLow.IsDisabled()
        xMarkerWaterLow.Enable()
    endif
    if xMarkerWaterHigh.IsEnabled()
        xMarkerWaterHigh.Disable()
    endif
    if xMarkerWaterBlood.IsEnabled()
        xMarkerWaterBlood.Disable()
    endif
    globalLeverWaterPulled.SetValue(0.0);
    globalLeverBloodPulled.SetValue(0.0);
    actor npc = akActionRef as actor
    npc.additem(UndressAmulet,1,true)
    npc.equipitem(UndressAmulet,false,true)
    npc.removeitem(UndressAmulet,1,true)
    xMarkerWarm.Disable()
    ModPlayerWetness(-530.0)     
EndEvent