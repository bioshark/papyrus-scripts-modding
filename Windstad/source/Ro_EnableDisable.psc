Scriptname Ro_EnableDisable extends ObjectReference  

Actor Property PlayerREF Auto
ObjectReference property marker Auto

Event OnActivate(ObjectReference akActionRef)    
    If akActionRef == PlayerREF
        if !marker.IsEnabled()
            marker.enable()
        else
            marker.disable()
        endif
    EndIf
EndEvent
