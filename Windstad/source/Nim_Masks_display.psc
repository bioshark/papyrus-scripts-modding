Scriptname Nim_Masks_display extends ObjectReference  

ObjectReference property Krosis auto
ObjectReference property Volsung auto
ObjectReference property Nahkriin auto
ObjectReference property Hevnoraak auto
ObjectReference property Otar auto
ObjectReference property Morokei auto
ObjectReference property Rahgot auto
ObjectReference property Vokun auto
ObjectReference property Konahrik auto
ObjectReference property Wooden auto
ObjectReference property Ahzidal auto
ObjectReference property Dukaan auto
ObjectReference property Zahkriisos auto
ObjectReference property Miraak00 auto
ObjectReference property Miraak01 auto
ObjectReference property Miraak02 auto
ObjectReference property Miraak03 auto
ObjectReference property Miraak04 auto
ObjectReference property Miraak05 auto
ObjectReference property Miraak06 auto

ObjectReference property Masks_chest auto


Form ArmorDragonPriestMaskBronzeHelmet
Form ArmorDragonPriestMaskCorondrumHelmet
Form ArmorDragonPriestMaskEbonyHelmet
Form ArmorDragonPriestMaskIronHelmet
Form ArmorDragonPriestMaskMarbleHelmet
Form ArmorDragonPriestMaskMoonstoneHelmet
Form ArmorDragonPriestMaskOrichalumHelmet
Form ArmorDragonPriestMaskSteelHelmet
Form ArmorDragonPriestMaskUltraHelmet
Form ArmorDragonPriestMaskWoodHelmet
Form DLC2ArmorAcolyteMaskFire
Form DLC2ArmorAcolyteMaskFrost
Form DLC2ArmorAcolyteMaskShock
Form DLC2MiraakMaskNew
Form DLC2MKMiraakMask1H
Form DLC2MKMiraakMask1L
Form DLC2MKMiraakMask2H
Form DLC2MKMiraakMask2L
Form DLC2MKMiraakMask3H
Form DLC2MKMiraakMask3L


Event OnInit()

       ArmorDragonPriestMaskBronzeHelmet = Game.GetFormFromFile(0x061cb9,"Skyrim.esm")
       ArmorDragonPriestMaskCorondrumHelmet = Game.GetFormFromFile(0x061cab,"Skyrim.esm")
       ArmorDragonPriestMaskEbonyHelmet = Game.GetFormFromFile(0x061ca5,"Skyrim.esm")
       ArmorDragonPriestMaskIronHelmet = Game.GetFormFromFile(0x061cc1,"Skyrim.esm")
       ArmorDragonPriestMaskMarbleHelmet = Game.GetFormFromFile(0x061cc2,"Skyrim.esm")
       ArmorDragonPriestMaskMoonstoneHelmet = Game.GetFormFromFile(0x061c8b,"Skyrim.esm")
       ArmorDragonPriestMaskOrichalumHelmet = Game.GetFormFromFile(0x061cc0,"Skyrim.esm")
       ArmorDragonPriestMaskSteelHelmet = Game.GetFormFromFile(0x061cc9,"Skyrim.esm")
       ArmorDragonPriestMaskUltraHelmet = Game.GetFormFromFile(0x061cd6,"Skyrim.esm")
       ArmorDragonPriestMaskWoodHelmet = Game.GetFormFromFile(0x061cca,"Skyrim.esm")
       DLC2ArmorAcolyteMaskFire = Game.GetFormFromFile(0x0240fe,"Dragonborn.esm")
       DLC2ArmorAcolyteMaskFrost = Game.GetFormFromFile(0x0240ff,"Dragonborn.esm")
       DLC2ArmorAcolyteMaskShock = Game.GetFormFromFile(0x024037,"Dragonborn.esm")
       DLC2MiraakMaskNew = Game.GetFormFromFile(0x029a62,"Dragonborn.esm")
       DLC2MKMiraakMask1H = Game.GetFormFromFile(0x039fa1,"Dragonborn.esm")
       DLC2MKMiraakMask1L = Game.GetFormFromFile(0x039d2b,"Dragonborn.esm")
       DLC2MKMiraakMask2H = Game.GetFormFromFile(0x039fa2,"Dragonborn.esm")
       DLC2MKMiraakMask2L = Game.GetFormFromFile(0x039d2e,"Dragonborn.esm")
       DLC2MKMiraakMask3H = Game.GetFormFromFile(0x039fa3,"Dragonborn.esm")
       DLC2MKMiraakMask3L = Game.GetFormFromFile(0x039d2f,"Dragonborn.esm")

    
EndEvent       

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
    
    If akBaseItem==ArmorDragonPriestMaskBronzeHelmet
        Krosis.enable()
    elseif akBaseItem==ArmorDragonPriestMaskCorondrumHelmet
        Volsung.enable()
    elseif akBaseItem==ArmorDragonPriestMaskEbonyHelmet
        Nahkriin.enable()
    elseif akBaseItem==ArmorDragonPriestMaskIronHelmet
        Hevnoraak.enable()
    elseif akBaseItem==ArmorDragonPriestMaskMarbleHelmet
        Otar.enable()
    elseif akBaseItem==ArmorDragonPriestMaskMoonstoneHelmet
        Morokei.enable()
    elseif akBaseItem==ArmorDragonPriestMaskOrichalumHelmet
        Rahgot.enable()
    elseif akBaseItem==ArmorDragonPriestMaskUltraHelmet
        Konahrik.enable()
    elseif akBaseItem==ArmorDragonPriestMaskSteelHelmet
        Vokun.enable()
    elseif akBaseItem==ArmorDragonPriestMaskWoodHelmet
        Wooden.enable()
    elseif akBaseItem==DLC2ArmorAcolyteMaskFire
        Ahzidal.enable()
    elseif akBaseItem==DLC2ArmorAcolyteMaskFrost
        Dukaan.enable()
    elseif akBaseItem==DLC2ArmorAcolyteMaskShock
        Zahkriisos.enable()
    elseif akBaseItem==DLC2MiraakMaskNew
        Miraak00.enable()
    elseif akBaseItem==DLC2MKMiraakMask1H
        Miraak01.enable()
    elseif akBaseItem==DLC2MKMiraakMask1L
        Miraak02.enable()
    elseif akBaseItem==DLC2MKMiraakMask2H
        Miraak03.enable()
    elseif akBaseItem==DLC2MKMiraakMask2L
        Miraak04.enable()
    elseif akBaseItem==DLC2MKMiraakMask3H
        Miraak05.enable()
    elseif akBaseItem==DLC2MKMiraakMask3L
        Miraak06.enable()
    endif
   
   
Endevent

Event OnItemRemoved(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
   
    
    If akBaseItem==ArmorDragonPriestMaskBronzeHelmet
        Krosis.disable()
    elseif akBaseItem==ArmorDragonPriestMaskCorondrumHelmet
        Volsung.disable()
    elseif akBaseItem==ArmorDragonPriestMaskEbonyHelmet
        Nahkriin.disable()
    elseif akBaseItem==ArmorDragonPriestMaskIronHelmet
        Hevnoraak.disable()
    elseif akBaseItem==ArmorDragonPriestMaskMarbleHelmet
        Otar.disable()
    elseif akBaseItem==ArmorDragonPriestMaskMoonstoneHelmet
        Morokei.disable()
    elseif akBaseItem==ArmorDragonPriestMaskOrichalumHelmet
        Rahgot.disable()
    elseif akBaseItem==ArmorDragonPriestMaskUltraHelmet
        Konahrik.disable()
    elseif akBaseItem==ArmorDragonPriestMaskSteelHelmet
        Vokun.disable()
    elseif akBaseItem==ArmorDragonPriestMaskWoodHelmet
        Wooden.disable()
    elseif akBaseItem==DLC2ArmorAcolyteMaskFire
        Ahzidal.disable()
    elseif akBaseItem==DLC2ArmorAcolyteMaskFrost
        Dukaan.disable()
    elseif akBaseItem==DLC2ArmorAcolyteMaskShock
        Zahkriisos.disable()
    elseif akBaseItem==DLC2MiraakMaskNew
        Miraak00.disable()
    elseif akBaseItem==DLC2MKMiraakMask1H
        Miraak01.disable()
    elseif akBaseItem==DLC2MKMiraakMask1L
        Miraak02.disable()
    elseif akBaseItem==DLC2MKMiraakMask2H
        Miraak03.disable()
    elseif akBaseItem==DLC2MKMiraakMask2L
        Miraak04.disable()
    elseif akBaseItem==DLC2MKMiraakMask3H
        Miraak05.disable()
    elseif akBaseItem==DLC2MKMiraakMask3L
        Miraak06.disable()
    endif


Endevent
