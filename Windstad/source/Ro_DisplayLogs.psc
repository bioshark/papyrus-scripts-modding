Scriptname Ro_DisplayLogs extends ObjectReference

Actor Property PlayerREF Auto
MiscObject Property BYOHMaterialLog Auto

Event OnCellAttach()
    self.GetBaseObject().SetName("(" + PlayerREF.GetItemCount(BYOHMaterialLog) + ")");
EndEvent

Event OnInit()
    self.GetBaseObject().SetName("(" + PlayerREF.GetItemCount(BYOHMaterialLog) + ")");
EndEvent
