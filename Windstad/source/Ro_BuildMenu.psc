Scriptname Ro_BuildMenu extends ObjectReference  

Message Property menuMain Auto
Message Property menuPay Auto
Message Property menuMisc Auto
Message Property menuSleeping Auto
Message Property menuDecor Auto

GlobalVariable Property globalAlchemyBuilt Auto
GlobalVariable Property globalLibraryBuilt Auto
GlobalVariable Property globalRitualBuilt Auto
GlobalVariable Property globalArmoryBuilt Auto
GlobalVariable Property globalSmithyBuilt Auto
GlobalVariable Property globalBedroomBuilt Auto
GlobalVariable Property globalThrallBought Auto
GlobalVariable Property globalMaidenBought Auto

MiscObject property gold001 Auto
MiscObject property materialStones Auto
MiscObject property materialSawnLogs Auto
MiscObject property materialNails Auto
MiscObject property materialGlass Auto
MiscObject property materialLocks Auto
MiscObject property materialFittings Auto
MiscObject property materialHinges Auto
MiscObject property materialClay Auto

ObjectReference Property blockerAlchemy Auto
ObjectReference Property blockerLibrary Auto
ObjectReference Property blockerRitual Auto
ObjectReference Property blockerArmory Auto
ObjectReference Property blockerSmithy Auto
ObjectReference Property blockerBedroom Auto

ObjectReference Property referenceBed Auto
ObjectReference Property referenceDecor Auto
ObjectReference property refIronMaidenOne Auto
ObjectReference property refIronMaidenTwo Auto
ObjectReference Property refGoreMarker Auto
ObjectReference Property refPositionMarkerLeft Auto
ObjectReference Property refPositionMarkerRight Auto

Actor Property actorMaidenOne Auto
Actor Property actorMaidenTwo Auto
Actor Property actorThrall Auto
Actor Property PlayerREF Auto

Sound Property soundRoomBuilt Auto
Sound Property soundBedTypeChange Auto
Sound Property soundDecorTypeChange Auto

Event OnActivate(ObjectReference akActionRef)
    If akActionRef == PlayerREF ; Only the player
        Menu()
    EndIf
EndEvent

Function Menu(Bool abMenu = True, Int aiButton = 0)
    While abMenu
        If aiButton != -1
            aiButton = menuMain.Show() ; Main Menu
            If aiButton == 0 ; Alchemy
                aiButton = buildRoom("Alchemy Room", blockerAlchemy, globalAlchemyBuilt, 8000, 32, 6, 36, 40, 5, 12, 14, 12);
            ElseIf aiButton == 1; Library
                aiButton = buildRoom("Library", blockerLibrary, globalLibraryBuilt, 6000, 36, 30, 72, 4, 6, 6, 22, 8);
                ; now show ritual room option if library has been built
                if globalLibraryBuilt.GetValue() == 1
                    setGlobalValue(globalRitualBuilt, 0.0);
                endif;
            ElseIf aiButton == 2; Ritual
                aiButton = buildRoom("Ritual Room", blockerRitual, globalRitualBuilt, 4000, 20, 6, 22, 8, 3, 6, 12, 8);
            ElseIf aiButton == 3; Armory
                aiButton = buildRoom("Armory", blockerArmory, globalArmoryBuilt, 5000, 30, 14, 56, 4, 5, 24, 10, 8);
                ; now show smithy room option if armory has been built
                if globalArmoryBuilt.GetValue() == 1
                    setGlobalValue(globalSmithyBuilt, 0.0);                    
                endif
            ElseIf aiButton == 4; Smithing
                aiButton = buildRoom("Smithing Room", blockerSmithy, globalSmithyBuilt, 6000, 46, 4, 30, 1, 9, 30, 20, 16);
            ElseIf aiButton == 5; Bedroom
                aiButton = buildRoom("Bedroom", blockerBedroom, globalBedroomBuilt, 12000, 46, 10, 60, 6, 9, 18, 20, 17);
            ElseIf aiButton == 6; Misc
                handleMiscMenu();
            ElseIf aiButton == 7; Exit
                abMenu = False ; Exit
            EndIf
        EndIf
    EndWhile
EndFunction

Function setGlobalValue(GlobalVariable gv, float value)
    gv.SetValue(value)
EndFunction

Function handleMiscMenu()
    int button = 0;
    bool showMisc = true;
    While showMisc
        If button != -1
            button = menuMisc.Show();
            if button == 0; buy thrall                
                hirePerson("Thrall", globalThrallBought, 1500, actorThrall, none, "Thrall already acquired!")
            elseif button == 1; buy maidens
                hirePerson("Maidens", globalMaidenBought, 5000, actorMaidenOne, actorMaidenTwo, "Take a bath first!")
            elseif button == 2; release Maidens
                releaseMaidens(2000);
            elseif button == 3; sleeping options
                button = handleChoices(0, menuSleeping, referenceBed, soundBedTypeChange);
            elseif button == 4; change decor
                button = handleChoices(1, menuDecor, referenceDecor, soundDecorTypeChange);
            else; nothing, go back
                showMisc = false;
            endif
        endif
    EndWhile
EndFunction

Function releaseMaidens(int gold)
    if (Game.GetPlayer().GetItemCount(gold001) >= gold)
        Game.GetPlayer().RemoveItem(gold001, gold)
        actorMaidenOne.disable();
        actorMaidenTwo.disable();
        globalMaidenBought.SetValue(0.0);
    else
        debug.MessageBox("You don't have enough gold for bribes and travel expenses!")
    endif
EndFunction

Function hirePerson(String personName, GlobalVariable gl, int price, Actor person, Actor extraPerson, String rejectMessage)
    if (gl.GetValue() == 0) ;only buy if we don't have one
        if (Game.GetPlayer().GetItemCount(gold001) >= price) ; and only if one has money
            if extraPerson != none
                person.MoveTo(refPositionMarkerLeft)
                extraPerson.MoveTo(refPositionMarkerRight)
                person.enable();
                extraPerson.enable()
                refGoreMarker.disable();
                refIronMaidenOne.SetOpen(true);
                refIronMaidenTwo.SetOpen(true);
            else
                person.enable();
            endif
            Game.GetPlayer().RemoveItem(gold001, price);
            gl.SetValue(1.0);
            debug.notification(personName + " acquired")
        else
             debug.MessageBox("You don't have enough gold!")
        endif
    else
        debug.MessageBox(rejectMessage)
    endif
EndFunction

int Function handleChoices(int choiceType, Message menu, ObjectReference marker, Sound changeSound)
    String[] changeOptions = new String[4];
    changeOptions[0] = "Bed"
    changeOptions[1] = "Coffins"
    changeOptions[2] = "Vampire Decor"
    changeOptions[3] = "Necromancer Decor"
    int button = menu.Show()    
    if button == 0
        changeSound.play(self)
        marker.enable()
        debug.notification(changeOptions[2 * choiceType + button] + " enabled")
    elseif button == 1
        changeSound.play(self)
        marker.disable()
        debug.notification(changeOptions[2 * choiceType + button] + " enabled")
    else; nothing
    endif;
    return button
EndFunction

int Function buildRoom(String roomName, ObjectReference blocker, GlobalVariable globalVar, int gold, int stones, int sawnLogs, int nails, int glass, int locks, int fittings, int hinges, int clay)
    int button = menuPay.Show(gold, stones, sawnLogs, nails, glass, locks, fittings, hinges, clay);
    If button == 0; Pay with gold
        ; check if you have enough gold and pay
        if (Game.GetPlayer().GetItemCount(gold001) >= gold)
            Game.GetPlayer().RemoveItem(gold001, gold)
            createTheRoom(roomName, blocker, globalVar)
        else            
            debug.MessageBox("You don't have enough gold!")
        endif
    ElseIf button == 1; Pay with materials
        if (checkMaterials(stones, sawnLogs, nails, glass, locks, fittings, hinges, clay))
            Game.GetPlayer().RemoveItem(materialStones, stones)
            Game.GetPlayer().RemoveItem(materialSawnLogs, sawnLogs)
            Game.GetPlayer().RemoveItem(materialNails, nails)
            Game.GetPlayer().RemoveItem(materialGlass, glass)
            Game.GetPlayer().RemoveItem(materialLocks, locks)
            Game.GetPlayer().RemoveItem(materialFittings, fittings)
            Game.GetPlayer().RemoveItem(materialHinges, hinges)
            Game.GetPlayer().RemoveItem(materialClay, clay)
            createTheRoom(roomName, blocker, globalVar)
        endif
    else ; nothing, go back
    EndIf
    return button;
EndFunction

Function createTheRoom(String roomName, ObjectReference blocker, GlobalVariable globalVar)
    soundRoomBuilt.play(self)
    setGlobalValue(globalVar, 1.0);            
    blocker.disable()
    debug.notification(roomName + " built")
EndFunction

bool Function checkMaterials(int stones, int sawnLogs, int nails, int glass, int locks, int fittings, int hinges, int clay)
    bool canPay = false;
    if (Game.GetPlayer().GetItemCount(materialStones) >= stones)        
        if (Game.GetPlayer().GetItemCount(materialSawnLogs) >= sawnLogs)
            if (Game.GetPlayer().GetItemCount(materialNails) >= nails)
                if (Game.GetPlayer().GetItemCount(materialLocks) >= locks)
                    if (Game.GetPlayer().GetItemCount(materialFittings) >= fittings)
                        if (Game.GetPlayer().GetItemCount(materialHinges) >= hinges)
                            if (Game.GetPlayer().GetItemCount(materialClay) >= clay)
                                canPay = true;
                            else
                                debug.MessageBox("You don't have enough Clay!")
                            endif
                        else
                            debug.MessageBox("You don't have enough Hinges!")
                        endif
                    else
                        debug.MessageBox("You don't have enough Iron Fittings!")
                    endif
                else
                    debug.MessageBox("You don't have enough Locks!")
                endif
            else
                debug.MessageBox("You don't have enough Nails!")
            endif
        else
            debug.MessageBox("You don't have enough Sawn Logs!")
        endif
    else
        debug.MessageBox("You don't have enough Quarried Stone!")
    endif    
    return canPay;
EndFunction
