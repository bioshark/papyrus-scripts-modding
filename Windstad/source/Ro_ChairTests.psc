Scriptname Ro_ChairTests extends ObjectReference

Actor Property PlayerREF Auto

int downUp = 1;

Event OnActivate(ObjectReference akActionRef)
    downUp += 1;
    if downUp % 2 != 0
        debug.MessageBox("get up");
    else
        debug.MessageBox("sit down");
    endif
EndEvent
