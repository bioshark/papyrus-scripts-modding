Scriptname Ro_DailySpecials extends ObjectReference  

GlobalVariable Property GameDay Auto

String[] Property daySpecials Auto

TextureSet Property textureSpecials Auto

Event OnCellAttach()
	SetDailySpecials(GameDay.GetValue() as int)
	RegisterForMenu("Sleep/Wait Menu")
EndEvent

Event OnMenuClose(String MenuName)
	SetDailySpecials(GameDay.GetValue() as int)
EndEvent

Event OnCellDetach()
	UnregisterForAllMenus()
EndEvent

Function SetDailySpecials(int theDay)
    textureSpecials.SetNthTexturePath(0, daySpecials[theDay % 7]);
    
	Disable();
	Enable();
EndFunction
