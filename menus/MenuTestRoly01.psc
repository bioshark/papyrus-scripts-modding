ScriptName MenuTestRoly01 Extends ObjectReference
 
Actor Property PlayerREF Auto
Message Property OptionsMESG Auto
 
Event OnActivate(ObjectReference akActionRef)
	If akActionRef == PlayerREF ; Only the player
		Int iButton = OptionsMESG.Show() ; Shows your menu.
		If iButton == 0  ; Mage
			Debug.Notification("Option 1")
		ElseIf iButton == 1 ; Thief
			Debug.Notification("Option 2")
		ElseIf iButton == 2 ; Warrior
			Debug.Notification("Option 3")
		EndIf
	EndIf
EndEvent