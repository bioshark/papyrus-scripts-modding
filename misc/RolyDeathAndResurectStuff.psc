ScriptName RolyDeathAndResurectStuff Extends ObjectReference

Actor Property PlayerREF Auto
Actor Property referenceMaiden Auto
GlobalVariable Property globalMaidenBought Auto

Event OnActivate(ObjectReference akActionRef)
    If akActionRef == PlayerREF ; Only the player
        referenceMaiden.Kill();
        globalMaidenBought.SetValue(0.0);
    EndIf
EndEvent

