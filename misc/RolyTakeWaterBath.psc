ScriptName RolyTakeWaterBath Extends ObjectReference

Actor Property PlayerREF Auto

ObjectReference property refWaterLow Auto
ObjectReference property refWaterHigh Auto
ObjectReference property refBloodWater Auto

Sound Property soundWater Auto

Event OnActivate(ObjectReference akActionRef) 
    If akActionRef == PlayerREF
        if refBloodWater.isEnabled()
            refBloodWater.disable();
        endif
        soundWater.play(self)
        refWaterLow.disable();
        refWaterHigh.enable();
    EndIf
EndEvent
