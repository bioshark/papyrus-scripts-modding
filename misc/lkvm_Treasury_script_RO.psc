Scriptname lkvm_Treasury_script_RO extends ObjectReference  


ObjectReference property Vaultloot01 auto
ObjectReference property Vaultloot02 auto
ObjectReference property Vaultloot03 auto
ObjectReference property Vaultloot04 auto
ObjectReference property Vaultloot05 auto
ObjectReference property Vaultloot06 auto
ObjectReference property VaultLoot07 auto
ObjectReference property VaultLoot08 auto
ObjectReference property VaultLoot09 auto
ObjectReference property Vaultloot10 auto
ObjectReference property Vaultloot11 auto
ObjectReference property Vaultloot12 auto
ObjectReference property Vaultloot13 auto
ObjectReference property Vaultloot14 auto
ObjectReference property Vaultloot15 auto
ObjectReference property Vaultloot16 auto

ObjectReference property GoldVault auto

Form gold001

int value = 0

int Function ValueChange(Form akBaseItem, int aiItemCount)
	{Returns the value change from moving [aiItemCount] of [akBaseItem]}
	If akBaseItem==gold001
		return aiItemCount
	EndIf
EndFunction

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	gold001 = Game.GetFormFromFile(0x00000f,"Skyrim.esm")
	
	if (akBaseItem!=gold001)
		RemoveItem(akBaseItem, aiItemCount, True, akSourceContainer)
		Debug.MessageBox("Gold Coins Only!");
	else
		value += ValueChange(akBaseItem, aiItemCount)
		if (value >= 1000)
			Vaultloot01.enable()
		elseif  (value <= 999)
			Vaultloot01.disable()
		endif
		if (value >= 5000)
			Vaultloot02.enable()
		elseif  (value <= 4999)
			Vaultloot02.disable()
		endif
		if (value >= 10000)
			Vaultloot03.enable()
		elseif  (value <= 9999)
			Vaultloot03.disable()
		endif
		if (value >= 25000)
			Vaultloot04.enable()
		elseif  (value <= 24999)
			Vaultloot04.disable()
		endif
		if (value >= 50000)
			Vaultloot05.enable()
		elseif  (value <= 49999)
			Vaultloot05.disable()
		endif
		if (value >= 100000)
			Vaultloot06.enable()
		elseif  (value <= 99999)
			Vaultloot06.disable()
		endif
		if (value >= 200000)
			VaultLoot07.enable()
		elseif  (value <= 199999)
			VaultLoot07.disable()
		endif
		if (value >= 300000)
			VaultLoot08.enable()
		elseif  (value <= 299999)
			VaultLoot08.disable()
		endif
		if (value >= 400000)
			VaultLoot09.enable()
		elseif  (value <= 399999)
			VaultLoot09.disable()
		endif
		if (value >= 500000)
			Vaultloot10.enable()
		elseif  (value <= 499999)
			Vaultloot10.disable()
		endif
		if (value >= 650000)
			Vaultloot11.enable()
		elseif  (value <= 649999)
			Vaultloot11.disable()
		endif
		if (value >= 800000)
			Vaultloot12.enable()
		elseif  (value <= 799999)
			Vaultloot12.disable()
		endif
		if (value >= 1000000)
			Vaultloot13.enable()
		elseif  (value <= 999999)
			Vaultloot13.disable()
		endif
		if (value >= 1250000)
			Vaultloot14.enable()
		elseif  (value <= 1249999)
			Vaultloot14.disable()
		endif
		if (value >= 1500000)
			Vaultloot15.enable()
		elseif  (value <= 1499999)
			Vaultloot15.disable()
		endif
		if (value >= 2000000)
			Vaultloot16.enable()
		elseif  (value <= 1999999)
			Vaultloot16.disable()
		endif
	endif
Endevent

Event OnItemRemoved(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
    
    value -= ValueChange(akBaseItem, aiItemCount)
    
    if (value >= 1000)
        Vaultloot01.enable()
    elseif  (value <= 999)
        Vaultloot01.disable()
    endif
    if (value >= 5000)
        Vaultloot02.enable()
    elseif  (value <= 4999)
        Vaultloot02.disable()
    endif
    if (value >= 10000)
        Vaultloot03.enable()
    elseif  (value <= 9999)
        Vaultloot03.disable()
    endif
    if (value >= 25000)
        Vaultloot04.enable()
    elseif  (value <= 24999)
        Vaultloot04.disable()
    endif
    if (value >= 50000)
        Vaultloot05.enable()
    elseif  (value <= 49999)
        Vaultloot05.disable()
    endif
    if (value >= 100000)
        Vaultloot06.enable()
    elseif  (value <= 99999)
        Vaultloot06.disable()
    endif
    if (value >= 200000)
        VaultLoot07.enable()
    elseif  (value <= 199999)
        VaultLoot07.disable()
    endif
    if (value >= 300000)
        VaultLoot08.enable()
    elseif  (value <= 299999)
        VaultLoot08.disable()
    endif
    if (value >= 400000)
        VaultLoot09.enable()
    elseif  (value <= 399999)
        VaultLoot09.disable()
    endif
    if (value >= 500000)
        Vaultloot10.enable()
    elseif  (value <= 499999)
        Vaultloot10.disable()
    endif
    if (value >= 650000)
        Vaultloot11.enable()
    elseif  (value <= 649999)
        Vaultloot11.disable()
    endif
    if (value >= 850000)
        Vaultloot12.enable()
    elseif  (value <= 749999)
        Vaultloot12.disable()
    endif
    if (value >= 1000000)
        Vaultloot13.enable()
    elseif  (value <= 999999)
        Vaultloot13.disable()
    endif
    if (value >= 1250000)
        Vaultloot14.enable()
    elseif  (value <= 1249999)
        Vaultloot14.disable()
    endif
    if (value >= 1500000)
        Vaultloot15.enable()
    elseif  (value <= 1499999)
        Vaultloot15.disable()
    endif
    if (value >= 2000000)
        Vaultloot16.enable()
    elseif  (value <= 1999999)
        Vaultloot16.disable()
    endif

Endevent

