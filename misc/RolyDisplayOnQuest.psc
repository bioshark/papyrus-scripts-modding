ScriptName RolyDisplayOnQuest Extends ObjectReference

Quest property questToCheck Auto

Int Property stageValueToCheck Auto

Event onCellAttach()
    if !self.IsEnabled()
        if questToCheck.GetStage() >= stageValueToCheck
            self.enable()
        endif
    endif
EndEvent
