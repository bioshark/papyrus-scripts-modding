ScriptName RolyPoolUnequip Extends ObjectReference

ObjectReference Property xMarkerWaterLow Auto
ObjectReference Property xMarkerWaterHigh Auto
ObjectReference Property xMarkerWaterBlood Auto

Actor Property PlayerREF Auto
Armor property JewelryRingGold auto

Armor Property kSlotMask30 Auto ; HEAD
Armor Property kSlotMask32 Auto ; BODY
Armor Property kSlotMask33 Auto ; Hands
Armor Property kSlotMask34 Auto ; Forearms
Armor Property kSlotMask37 Auto ; Feet
Armor Property kSlotMask38 Auto ; Calves
Armor Property kSlotMask39 Auto ; SHIELD
Armor Property kSlotMask40 Auto ; TAIL

Event OnTriggerEnter(ObjectReference akActionRef) 
    debug.notification("enter area!");
    if akActionRef == PlayerREF
        kSlotMask30 = Game.GetPlayer().GetWornForm(0x00000001) as Armor
        kSlotMask32 = Game.GetPlayer().GetWornForm(0x00000004) as Armor
        kSlotMask33 = Game.GetPlayer().GetWornForm(0x00000008) as Armor
        kSlotMask34 = Game.GetPlayer().GetWornForm(0x00000010) as Armor
        kSlotMask37 = Game.GetPlayer().GetWornForm(0x00000080) as Armor
        kSlotMask38 = Game.GetPlayer().GetWornForm(0x00000100) as Armor
        kSlotMask39 = Game.GetPlayer().GetWornForm(0x00000200) as Armor
        kSlotMask40 = Game.GetPlayer().GetWornForm(0x00000400) as Armor
        PlayerRef.unequipItem(kSlotMask30, false, true);
        PlayerRef.unequipItem(kSlotMask32, false, true);
        PlayerRef.unequipItem(kSlotMask33, false, true);
        PlayerRef.unequipItem(kSlotMask34, false, true);
        PlayerRef.unequipItem(kSlotMask37, false, true);
        PlayerRef.unequipItem(kSlotMask38, false, true);
        PlayerRef.unequipItem(kSlotMask39, false, true);
        PlayerRef.unequipItem(kSlotMask40, false, true);
    else
        actor npc = akActionRef as actor
        npc.unequipall();
    endif
EndEvent


Event OnTriggerLeave(ObjectReference akActionRef) 
    if xMarkerWaterLow.IsDisabled()
        xMarkerWaterLow.Enable()
    endif
    if xMarkerWaterHigh.IsEnabled()
        xMarkerWaterHigh.Disable()
    endif
    if xMarkerWaterBlood.IsEnabled()
        xMarkerWaterBlood.Disable()
    endif
    if akActionRef == PlayerREF
        debug.notification("leaving area!")
        PlayerRef.equipitem(kSlotMask30, false, true)
        PlayerRef.equipitem(kSlotMask32, false, true)
        PlayerRef.equipitem(kSlotMask33, false, true)
        PlayerRef.equipitem(kSlotMask34, false, true)
        PlayerRef.equipitem(kSlotMask37, false, true)
        PlayerRef.equipitem(kSlotMask38, false, true)
        PlayerRef.equipitem(kSlotMask39, false, true)
        PlayerRef.equipitem(kSlotMask40, false, true)
        kSlotMask30 = none;
        kSlotMask32 = none;
        kSlotMask33 = none;
        kSlotMask34 = none;
        kSlotMask37 = none;
        kSlotMask38 = none;
        kSlotMask39 = none;
        kSlotMask40 = none;
    else
        actor npc = akActionRef as actor
        npc.additem(jewelryringgold,1,true)
        npc.equipitem(jewelryringgold,false,true)
        npc.removeitem(jewelryringgold,1,true)     
    endif
EndEvent
