Scriptname lkvm_seat extends ObjectReference

ObjectReference Property ChairMarkerREF Auto
Actor Property PlayerRef Auto
Sound Property AMBWaterDripsSplashLP Auto


Event OnActivate(ObjectReference akActionRef)
if akActionRef == PlayerREF
ChairMarkerREF.Activate(PlayerREF)
if AMBWaterDripsSplashLP
int test = AMBWaterDripsSplashLP.Play(Self)
Sound.SetInstanceVolume(test,2)
Utility.Wait(7.0)
Sound.SetInstanceVolume(test,0.1)
endif
endif
EndEvent